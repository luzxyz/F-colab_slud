FROM node:lts-alpine

WORKDIR /app

RUN apk update
RUN apk upgrade

COPY package.json .

RUN yarn install

COPY . .
RUN yarn build

EXPOSE 3000

ENV NUXT_HOST=0.0.0.0

ENTRYPOINT [ "yarn" ]
CMD [ "start" ]